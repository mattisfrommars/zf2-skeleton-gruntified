<?php

namespace ApplicationTest\Service;

use PHPUnit_Framework_TestCase as TestCase;
use Application\Service\ApplicationService;
use ApplicationTest\Bootstrap;

/**
 * Description of ApplicationServiceTest
 *
 * @author pedro
 */
class ApplicationServiceTest extends TestCase
{

    /**
     * @var Application\Service\ApplicationService
     */
    protected $applicationService;

    /**
     * @var \Zend\ServiceManager\ServiceManager
     */
    protected $serviceManager;

    public function setUp()
    {
        parent::setUp();

        $this->applicationService = $this->getMockBuilder('Application\Service\ApplicationService')
                ->disableOriginalConstructor()
                ->getMock();

        $this->serviceManager = Bootstrap::getServiceManager();
        $this->serviceManager->setAllowOverride(true)
                ->setService('Config', include(realpath('../config/module.config.php')));
        $this->serviceManager->setService('Application\Service\Application', $this->applicationService);
    }

    public function testApplicationServiceImplementsServiceLocatorAwareInterface()
    {
        $this->assertInstanceOf('Zend\ServiceManager\ServiceLocatorAwareInterface', $this->applicationService);
    }

}
