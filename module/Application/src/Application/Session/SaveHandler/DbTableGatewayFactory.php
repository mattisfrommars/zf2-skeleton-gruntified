<?php

namespace Application\Session\SaveHandler;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Session\SaveHandler\DbTableGateway;
use Zend\Session\SaveHandler\DbTableGatewayOptions;

class DbTableGatewayFactory implements FactoryInterface
{

    /**
     * Create handler to save session on database
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \Zend\Session\SaveHandler\DbTableGateway
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $adapter = $serviceLocator->get('Zend\Db\Adapter\Adapter');
        $tableGateway = new TableGateway('session', $adapter);
        return new DbTableGateway($tableGateway, new DbTableGatewayOptions());
    }

}
